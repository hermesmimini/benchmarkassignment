package data;

import javax.ejb.Local;

@Local
public interface DataAccessInterface<T> {
	
	public T searchBible(String book, int chapter, int verse);
}
