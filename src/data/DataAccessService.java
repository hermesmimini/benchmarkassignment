package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import model.Verse;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class DataAccessService implements DataAccessInterface<Verse> {
	Connection conn = null;
	String url = "jdbc:mysql://localhost:3306/benchmark";
	String username = "root";
	String password = "root";

	@Override
	public Verse searchBible(String book, int chapter, int verseNumber) {
		String sql = String.format("SELECT * FROM NIV WHERE BOOK='%s' AND CHAPTER='%d' AND verseNumber='%d'", book, chapter, verseNumber);
		Verse returnVerse = null; 
		
		try{
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				returnVerse = new Verse(rs.getString("BOOK"), rs.getInt("CHAPTER"), rs.getInt("verseNumber"), rs.getString("verseContent"));
			}
		}
		
		catch(SQLException e){
			e.printStackTrace();
		}
		return returnVerse;
	}

}
