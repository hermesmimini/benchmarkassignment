package model;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Verse {
	private String book;
	private int chapter;
	private int verseNumber;
	private String verseContent;
	
	public Verse() {
		book = "";
		chapter = 0;
		verseNumber = 0;
		verseContent = "";
	}

	public Verse(String book, int chapter, int verseNumber, String verseContent) {
		super();
		this.book = book;
		this.chapter = chapter;
		this.verseNumber = verseNumber;
		this.verseContent = verseContent;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public int getChapter() {
		return chapter;
	}

	public void setChapter(int chapter) {
		this.chapter = chapter;
	}

	public int getVerseNumber() {
		return verseNumber;
	}

	public void setVerseNumber(int verseNumber) {
		this.verseNumber = verseNumber;
	}

	public String getVerseContent() {
		return verseContent;
	}

	public void setVerseContent(String verseContent) {
		this.verseContent = verseContent;
	}
	
	
	
	

}
