package business;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import data.DataAccessInterface;
import model.Verse;


@Stateless
@Local(BibleBusinessInterface.class)
@LocalBean
public class BibleBusinessService implements BibleBusinessInterface<Verse> {

	//Inject Service
	@Inject
	DataAccessInterface<Verse> service;
	
	@Override
	public Verse searchBible(String book, int chapter, int verse) {
		return service.searchBible(book, chapter, verse);
	}

}
