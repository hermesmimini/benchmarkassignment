package business;

import javax.ejb.Local;

@Local
public interface BibleBusinessInterface<T> {
	
	/**
	 * This method will go through the database and return a Verse object 
	 * where they have the given parameters. 
	 * 
	 * @param book - String Class (Name of the book where the verse is located in.)
	 * @param chapter - Integer Class (Number of the chapter where the verse is located in.)
	 * @param verseNumber - Integer Class (NUmber of the verse itself.)
	 * @return Verse Class - (Verse Object that corresponds to those given parameters.)
	 */
	public T searchBible(String book, int chapter, int verseNumber);
}
