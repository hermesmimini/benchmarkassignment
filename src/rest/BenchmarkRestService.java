package rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.Verse;
import business.BibleBusinessInterface;

@RequestScoped
@Path("/bible")
public class BenchmarkRestService {

	@Inject
	BibleBusinessInterface<Verse> service;
	
	@GET
	@Path("/findVerse/{book}/{chapter}/{verse}")
	@Produces(MediaType.APPLICATION_JSON)
	public Verse findVerse(@PathParam("book") String book, @PathParam("chapter") int chapter, @PathParam("verse") int verseNumber)
	{
		return service.searchBible(book, chapter, verseNumber);
	}
	
}
